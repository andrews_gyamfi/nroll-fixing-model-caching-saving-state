var app = require("./app/app"),
    api = require("./app/api/api"),
    express = require("express"),
    config = require("./app/config/config"),
    emailVerification = require("./app/services/emailVerification"),
    mongoose = require("mongoose");

//emailVerification.send("andrewsgyamfi@ymail.com");
mongoose.Promise = global.Promise;

//Initialize mongodb
mongoose.connect(config.mongo.url);
// TODO: initialize middlewares here



app.use(express.static("./frontend"));

app.use("/api", api);

app.get("/", function (req, res, next) {
    res.sendFile(__dirname + "/frontend/views/base.html");
});

app.use(function (err, req, res, next) {
    console.log(err);
})
app.listen(config.port);