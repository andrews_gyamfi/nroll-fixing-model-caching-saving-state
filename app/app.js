/**
 * App.js routes all the pieces together for a complete working application.
 * Route is define for Application, Review & User objects
 */
var express = require("express"),
    app = express(),
    api = require("./api/api"),
    authenticator = require("./services/passportAuth"),
    middlewares = require("./middleware/middlewares")(app);

module.exports = app;