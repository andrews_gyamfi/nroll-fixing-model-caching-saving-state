var User = require('../api/user/userModel'),
    bodyParser = require("body-parser"),
    passport = require("passport"),
    LocalStrategy = require("passport-local").Strategy,
    config = require('../config/config');

function Authenticator(app) {

    /* use passport as middleware */
    app.use(passport.initialize());

    /* serialize user */
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    })

    /*-----------------------------------------------------------*/
    /* Local strategies for handling registration and login */
    /*-----------------------------------------------------------*/

    /* Strategy options */
    var strategyOptions = {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
        session: false
    };

    /**
     * Local login strategy
     */
    var loginStrategy = new LocalStrategy(strategyOptions, function (req, email, password, done) {
        User.findOne({
            email: email
        }, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            } else if ((!user.comparePasswords(password))) {
                return done(null, false);
            } else {
                return done(null, user);
            }
        });
    });

    /**
     * Passport register strategy
     */
    var registerStrategy = new LocalStrategy(strategyOptions, function (req, email, password, done) {
        User.findOne({
            email: email
        }, function (err, user) {

            if (err) {
                return done(err);
            }

            if (user) {
                return (done, false, {
                    message: "An account already exists for your email"
                });
            }

            if (!user) {
                var userInfo = {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: req.body.password
                }

                User.create(userInfo, function (err, user) {
                    if (err) {
                        return done(err);
                    } else {
                        return done(null, user);
                    }
                });
            }
        })
    });

    /* Use strategies */
    passport.use("local-login", loginStrategy);
    passport.use("local-register", registerStrategy);

}

module.exports = Authenticator;