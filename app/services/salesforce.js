/**
 * Module Handles Salesforce API connection
 * 1. Initiates salesforce login
 * 2. Creates new application based on form input
 * 3. Get all courses 
 * 4. Get intakes
 */

var jsForce = require("jsforce"),
    config = require("../config/config");



function Salesforce() {
    /* API access details */
    var userLogins = {
        username: config.salesforce.username,
        password: config.salesforce.password,
        token: config.salesforce.accessToken
    };

    /* Initiate connection */
    var con = new jsForce.Connection();

    /* Attempt login */
    con.login(userLogins.username, userLogins.password + userLogins.token, function (err, res) {
        if (err) {
            console.log(userLogins.username + " " + userLogins.password);
            console.log('Could not login - ' + err);
        }
    });

    /* Create new application */
    function createNewApp(data, cb) {
        con.sobject("Application__c").create(data, function (err, ret) {
            if (err || !ret.success) {
                return console.error(err, ret);
            }
            console.log("Created record id : " + ret.id);
            return cb(ret.id);
        });
    };


    /* Create new Hire agreement */
    function createHireAgreement(data, cb) {
        con.sobject("Hire_Agreement_2__c").create(data, function (err, ret) {
            if (err || !ret.success) {
                return console.error(err, ret);
            }
            console.log("Created record id : " + ret.id);
            return cb(ret.id);
        });
    }


    /* Get all delivered courses */
    function getAllCourses(cb) {
        con.sobject("Product2")
            .select("Id, ProductCode, Name")
            .where("isActive = TRUE")
            .execute(function (err, data) {
                if (err) {
                    return console.err(err);
                }
                return cb(data);
            });
    }

    /* Gets all intakes */
    function getAllIntakes(cb) {
        con.sobject("Intake__c")
            .find({})
            .select("Id, Name, Online_Orientation_day__c, Course__r.Id, Course__r.ProductCode, Course__r.Name, Course__r.Industry__c,  Campus__r.Name, Campus__r.Id")
            .execute(function (err, data) {
                if (err) {
                    return console.err(err);
                }
                return cb(data);
            })
    }

    /* Get intakes in selected course */
    function getIntakesByCourse(courseId, cb) {
        con.sobject("Intake__c")
            .find({
                Course__c: courseId
            })
            .select("Id, Name, Online_Orientation_day__c, Course__c, Campus__r.Name")
            .execute(function (err, data) {
                if (err) {
                    return console.err(err);
                }
                return cb(data);

            });
    }

    /* Method stores files */
    function addAttachment(fileName, base64Data, fileType, userId, cb) {
        con.sobject('Attachment').create({
                ParentId: userId,
                Name: fileName,
                Body: base64Data,
                ContentType: fileType,
            },
            function (err, uploadedAttachment) {
                if (err) {
                    console.log("ERROR: " + err);
                }

                if (uploadedAttachment) {
                    cb(fileName);
                } else {
                    cb(false);
                }

            });
    }


    return {
        addAttachment: addAttachment,
        createNewApp: createNewApp,
        createHireAgreement: createHireAgreement,
        getAllCourses: getAllCourses,
        getIntakesByCourse: getIntakesByCourse,
        getAllIntakes: getAllIntakes
    };


}


module.exports = Salesforce;