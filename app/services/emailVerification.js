var _ = require("lodash"),
    fs = require("fs"),
    jwt = require("jsonwebtoken"),
    nodemailer = require("nodemailer"),
    user = require("../api/user/userModel"),
    smtpTransport = require("nodemailer-smtp-transport"),
    config = require("../config/config"),
    path = require("path");


/**
 * Object containing elements to be replaced in email template before sending
 * to user 
 * Feel free to add as required
 */
var model = {
    verifyUrl: "https://2ae72b14.ngrok.io/api/auth/verifyEmail?token=",
    title: "Enrol",
    subTitle: "We only work with human!",
    body: "Please verify your email by clicking the button below",
    firstName: "Andrews"
};

/**
 * Function reads email template file and interpolates respective data fields
 * @param token token to be inserted into link button in email
 * @returns template file 
 */

function getHtml(token) {
    var filePath = path.normalize(__dirname + "/../views/verify.html"),
        html = fs.readFileSync(filePath, "utf8");

    var template = _.template(html);

    return template(model);

}

/**
 * Load template settings for lodash for interpolation 
 */
_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

/**
 * Method sends emeail
 * @param email address of receipient 
 */
exports.send = function (email) {

    var payload = {
        sub: email
    };

    var token = jwt.sign(payload, config.secrets.jwt);

    model.verifyUrl += token;

    var transporter = nodemailer.createTransport(smtpTransport({
        host: "smtp.mailgun.org",
        secure: true,
        auth: {
            user: config.mailgun.user,
            pass: config.mailgun.pass
        }
    }));


    var mailOptions = {
        from: "andrews.gyamfi@digitalbunker.com.au",
        to: email,
        subject: "Enroll Account Verification",
        html: getHtml("234rerwrwer")
    };

    console.log(mailOptions.html);

    transporter.sendMail(mailOptions, function (err, info) {
        if (err) console.log(err);
        console.log("Email sent: " + info);
    })
}


exports.verify = function (req, res, next) {
    var token = req.query.token,
        payload = jwt.verify(token, config.secrets.jwt);

    console.log("********TOKEN**********:" + req.query.token);

    var email = payload.sub;

    if (!email) console.log("no email available");

    User.findOne({
        email: email
    }, function (err, user) {
        if (err) console.log(err);

        if (!user.active) {
            user.active = true;
        }

        user.save(function (err) {
            if (err) {
                console.log("Could not save user!");
            } else {
                res.send("Your email account " + email + " " + " has been activated");
            }

        })

    })


}