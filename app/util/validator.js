/**
 * Handles input validation
 */
var validate = require("express-validator"),
    _ = require("lodash");

/**
 * Method handles form validation
 */
var Validator = function (req, res, next) {

    /* Declare arrays to hold different input types */
    var rawData = req.body;

    inputTypes = {
        onlyStringInput: [],
        onlyNumericInput: [],
        onlyDateInput: [],
        alphaNumericInput: [],
        booleanInput: [],
        invalidInput: []
    };

    /* Array of all string inputs */
    inputTypes.onlyStringInput = {
        noSpacesAllowed: [
            "personal.title",
            "personal.firstName",
            "personal.lastName",
            "personal.previouFirstName",
            "personal.previouLastName",
            "personal.gender",
            "contact.address.residential.state",
            "contact.address.postal.suburb",
            "contact.address.postal.state",
            "emergency.relationship",
            "educational.highSchool.state"
        ],
        spacesAllowed: [
            "educational.highestLevel",
            "contact.address.residential.suburb",
            "contact.address.residential.suburb",
            "contact.address.residential.street",
            "contact.address.postal.street",
            "personal.culturalBackground",
            "emergency.name",
            "personal.countryOfBirth",
            "citizenship.country",
            "citizenship.visaStatus",
            "citizenship.otherVisa.subCategory",
            "educational.highSchool.name",
            "educational.previousQuals",
            "employment.employmentStatus",
            "employment.occupation",
            "employment.industry",
            "other.language",
            "other.englishLevel",
            "other.disabilityType",
            "other.studyReason",
            "other.otherStudyReason",
            "educational.recentVicShool",
            "funding.vfh.recentProvider.name"
        ]

    };

    /* Array of numeric inputs e.g. postal code, phone numbers, etc. */
    inputTypes.onlyNumericInput = [
            "educational.vsn",
            "contact.address.residential.postcode",
            "contact.address.postal.postcode",
            "contact.mobile",
            "emergency.number",
            "educational.highSchool.lui"
    ];

    /* Array of date inputs e.g. date of birth, year of arrival, etc. */
    inputTypes.onlyDateInput = [
        "personal.dateOfBirth",
        "personal.yearOfArrival",
        "educational.highSchool.year",
        "citizenship.otherVisa.expiryDate",
        "funding.vfh.recentProvider.yearLastEnrolled"
    ]

    /* Array of alpha numeric inputs e.g. street address */
    inputTypes.alphaNumericInput = [
        "rawData.other.usi",
        "funding.vfh.recentProvider.studentNumber",
        "funding.vfh.CHESSN"
    ];

    /* Array of boolean inputs */
    inputTypes.booleanInput = [
        "personal.hasPreviousName",
        "contact.address.same",
        "funding.vfh.wantsToAccess",
        "funding.vfh.hasRecentProvider",
        "funding.vfh.previouslyAccessedVfh",
        "educational.attendedSchool",
        "educational.stillAttending",
        "educational.attendingYearTwelve",
        "educational.attendedVicSchool",
        "educational.attendedVet",
        "educational.hasVsn",
        "other.hasUsi",
        "other.isDisabled",
        "other.disabilityType",
        "other.otherDisabilityType",
        "other.hasDisabilityPlan",
        "other.disabilityManagementPlan",
        "citizenship.otherVisa.allowedToStudy",
        "citizenship.scvVisa"
    ]

    var validateBoolean = function () {
        req.checkBody(inputTypes.booleanInput).isBoolean();
    }

    /* Method validates personal info 
    var validatePersonalInfo = function () {

        _.forEach(rawData.personal, function (value, key) {
            if (key === "gender") {
                req.checkBody("personal." + key, "Please select the right gender").isAlpha.isLength({
                    min: 1,
                    max: 6
                });
            }

            if (key === "title" || key === "firstName" || key === "lastName" || key === "previousFirstName" || key === "previousLastName") {
                req.checkBody("personal." + key, "At least 2 characters required").isAlpha().isLength({
                    min: 2,
                    max: 60
                });
            }

            if (key === "countryOfBirth") {
                req.checkBody("personal." + key, "Wrong input for country").isAscii().isLength({
                    min: 4,
                    max: 100
                });
            }

            if (key === "countryOfBirth" && value !== "Australia") {
                req.checkBody("personal." + 'culturalBackground').optional();

            } else {
                req.checkBody("personal." + 'yearOfArrival').optional();
            }

            if (key === "dateOfBirth") {
                req.checkBody("personal." + key, "Please enter a valid date").isDate();
            }

            if (key === "hasPreviousName") {

            }
        })
    }
    */

    /**
     * Method loops through all inputs where spaces IS NOT ALLOWED to make sure this data is as expected
     * 1. Names are expected to have a minimum length of 2 and maximum length of 60
     
    var validateNoSpaces = function () {
        _.forEach(inputTypes.onlyStringInput.noSpacesAllowed, function (value, key) {
            if (value === null) {
                console.log("ignored: " + key);
                return;
            }

            console.log("VALUE: " + value + " - " + "KEY: " + key);

            if (req.checkBody(key, "Please check you entered").isAlpha()) {
                if (key === "firstName" || key === "lastName" || key === "previousFirstName" || key === "previousLastName") {
                    req.checkBody(key, key + " " + "At least 2 characters required").isLength({
                        min: 2,
                        max: 60
                    });
                }
            }
        });
    }
    */

    /**
     * Method loops through all inputs where spaces IS ALLOWED to make sure this has not been completed by a bot
     */
    var validateSpacesAllowed = function () {
        _.forEach(inputTypes.onlyStringInput.spacesAllowed, function (value, key) {
            if (value === null) {
                console.log("ignored: " + key);
                return;
            }
            req.checkBody(key, "Invalid String").isAscii();
        });
    }

    /**
     * Method loops through all numeric inputs and validate as necessary
     * 1. Post codes should have a minimum length of 4 and max of 5 and should be only numeric characters
     * 2. Contact numbers are validated based on Australian requirements
     
    var validateNumericInput = function () {
        _.forEach(inputTypes.onlyNumericInput, function (value, key) {
            if (value === null) {
                console.log("ignored: " + key);
                return;
            }
            if (req.checkBody(key, "Please check your input").isNumeric()) {
                if (key === "postcode" || key === "postalPostcode") {
                    req.checkBody(key, "Post code should not be more than 5 numbers").isLength({
                        min: 4,
                        max: 5
                    });
                } else if (key === "primaryPhone" || key === "emergencyContactNumber") {
                    req.checkBody(key, "Please check the phone number you entered").isMobilePhone("en-AU");
                }

            }
        })
    } */


    /**
         * Method loops through all inputs where spaces IS ALLOWED to make sure this has not been completed by a bot
    
        var validateDateInput = function () {
            _.forEach(inputTypes.onlyDateInput, function (value, key) {
                req.checkBody(key, "Please check the date you entered").isDate();
                if (key === "dateOfBirth") {

                }
            });
        } */

    /* Transform radio options to boolean 
    var booleanize = function () {
        _.forEach(inputTypes.booleanInput, function (value, key) {
            if (value === null) {
                return;
            }
            if (value === "yes") {
                req.body[key] = true;
            } else if (value === "no") {
                req.body[key] = undefined
            } else {
                req.body[key] = false;
            }
        })
    }*/

    var prepInputLogic = function () {

        /* Function trims and convert string to lowercase */
        var cleanStr = function (str) {
            if (str) {
                return str.toLowerCase().trim();
            }
        }

        /* If person has no previous name, do not require previous name fields */
        if (!rawData.personal.hasPreviousName) {
            req.checkBody(["personal.previousFirstName", "personal.previousLastName"]).optional();
        }

        /* If person has same postal address, do not require postal address fields */
        if (rawData.contact.address.same) {
            req.checkBody(["contact.address.postal.street", "contact.address.postal.postcode", "contact.address.postal.state", "contact.address.postal.suburb"]).optional();
        }

        /* If country of birth is not Australia, require arrival date. Otherwise require cultural background */
        if (cleanStr(rawData.personal.countryOfBirth) === cleanStr("Australia")) {
            req.checkBody("personal.yearOfArrival").optional();
        } else {
            req.checkBody("personal.culturalBackground").optional();
        }

        /* If did not attend high school, do not require associated fields */
        if (cleanStr(rawData.educational.highestLevel) === "Never Attended School".toLowerCase()) {
            req.checkBody(["educational.highSchool.name", "educational.highSchool.state", "educational.highSchool.year"]).optional();
        }
        /* If never employed, don't require employment details, otherwise require employment details as necessary */
        if (cleanStr(rawData.employment.employmentStatus) !== "Full time employerr" || cleanStr(rawData.employment.employmentStatus) !== "Part time employerr") {
            req.checkBody(["employment.occupation", "employment.industry"]).optional();

        }

        /* If not disabled, don't require disability details */
        if (!rawData.other.isDisabled) {
            req.checkBody(["other.disabilityType", "other.otherDisabilityType", "other.hasDisabilityPlan", "other.disabilityManagementPlan"]).optional();
        }

        /* LUI 
        if (cleanStr(req.body.attendingSchool) === "yes" && cleanStr(inputTypes.onlyStringInput.noSpacesAllowed.schoolState) === "QLD") {
            inputTypes.onlyStringInput.noSpacesAllowed.lui = null;
        }*/

        return true;
    }

    /* Run validation after form logic is complete */
    if (prepInputLogic()) {
        validateBoolean();
        /*        validatePersonalInfo();*/
        /*        validateNoSpaces();
                validateSpacesAllowed();*/
        /* validateNumericInput();
         validateDateInput();
         booleanize();*/
    }

    var error = req.validationErrors();
    if (error) {
        console.log(error);
    }

    next();

}

module.exports = Validator;