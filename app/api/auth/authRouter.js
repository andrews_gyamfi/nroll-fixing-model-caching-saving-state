/**
 * AuthRouther handles all things relating to registration, login and email verification
 */

/* Declare variables */
var router = require("express").Router(),
    User = require("../user/userModel"),
    config = require("../../config/config"),
    jwt = require("jsonwebtoken"),
    passport = require("passport"),
    EmailManager = require("../../services/emailVerification"),
    LocalStrategy = require("passport-local");

/**
 * Registration handler
 * 1. Checks if user already exists, return error message 
 * 2. Otherwise it attempts to create user object in database 
 * 3. The email managere sends an email to the newly created user to verify email address 
 */
router.post("/register", function (req, res, next) {

    /* Use passport.authenticate method to attempt login */
    passport.authenticate("local-register", {
        session: false,
    }, function (err, user) {

        /* Return error if error */
        if (err) {
            return next(err);
        }

        /* If user already exists, return error message */
        if (!user) {
            res.status(401).send({
                success: false,
                message: "An account already exists with this email"
            });


        } else {
            /* Otherwise create and send token to app */
            createAndSendToken(user.toJSON(), res);

            /* Email user to verify email address */
            EmailManager.send(user.email);
        }

    })(req, res, next);

});

/**
 * Login Handler 
 * 1. Attempts to log in user using local passport strategy
 */

router.post("/login", function (req, res, next) {

    passport.authenticate("local-login", {
        session: true
    }, function (err, user, info) {

        /* Return error if error */
        if (err) {
            return res.status(401).send({
                success: false
            });
        }

        /* Attempt to login */
        req.login(user, function (err) {
            if (err) {
                return res.status(401).send({
                    success: false
                });
            }

            /* Create and send token to app */
            createAndSendToken(user.toJSON(), res);
        })


    })(req, res, next);

});


/**
 * Email verification handler
 * Grabs token off url params and attempt to authenticate it 
 * If valid, sets user's active property to true.
 */

router.get("/verifyEmail", function (req, res, next) {

    /* Get token off query string */
    var token = req.query.token;

    /* Decode token */
    var payload = jwt.verify(token, config.secrets.jwt);

    /* Grab email off decoded token */
    var email = payload.sub;

    /* If email not found, return error message */
    if (!email) console.log("no email available");

    /* If email, attempt to retrieve user object */
    User.findOne({
        email: email
    }, function (err, user) {

        /* Log error if error */
        if (err) console.log(err);

        /* If user is not active, set active to true */
        if (!user.active) {
            user.active = true;
            user.save(function (err) {
                if (err) {
                    console.log("Could not save user!");
                } else {
                    res.send("Your email account " + email + " " + " has now been verified.");
                }

            })
        } else {
            res.send("Your account has already been verifed");
        }



    });

});

/** 
 * Method creates web tokens based on user id
 */
function createAndSendToken(user, res) {
    /* Token payload */
    var payload = {
        sub: user._id
    };

    /* Create token */
    var token = jwt.sign(payload, config.secrets.jwt);
    return res.json({
        success: true,
        user: user,
        token: token
    });
}


module.exports = router;