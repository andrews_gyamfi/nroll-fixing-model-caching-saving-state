/**
 * Router for retrieving courses from salesforce
 */
var router = require("express").Router(),
    controller = require("./courseController");

/* Get all courses from salesforce */
router.get("/intakes", controller.getAllIntakes);


module.exports = router;