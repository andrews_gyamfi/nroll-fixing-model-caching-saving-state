/**
 * This module defines the Schema & model for the pre-training review object. 
 * It's recommended that every information on the hardcopy pre-training review is 
 * capture here. 
 */

//Require mongoose
var mongoose = require("mongoose");

//Get mongoose Schema
var Schema = mongoose.Schema;

//Create a new Schema for the application form
var reviewSchema = new Schema({
    questions: {
        one: String,
        two: String,
        three: String,
        four: String,
        five: String
    },
    application: {
        type: Schema.Types.ObjectId,
        ref: 'application'
    }
});

//Export model for use within app
module.exports = mongoose.model("review", reviewSchema);