/**
 * User object model definition
 * This will hold minimal information for users to be able to login and access application details
 */

//Require mongoose
var mongoose = require("mongoose"),
    bcrypt = require("bcrypt-nodejs");

//Get mongoose Schema
var Schema = mongoose.Schema;

/* Application status */
var applicationStatuses = [
    "completed",
    "saved"
];

//Create a new Schema for the application form
var UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: false
    },
    applications: [
        {
            courseCode: String,
            courseName: String,
            status: String,
            sfId: String,
            appRefId: [Schema.Types.ObjectId],
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ]
});


UserSchema.virtual("hasApplications").get(function () {
    return this.applications.length;
})


UserSchema.pre('save', function (next) {
    var user = this;

    if (!this.isModified('password') || !this.isNew) return next();

    var hashedPassword = this.encryptPassword(this.password);
    this.password = hashedPassword;

    next();
})

UserSchema.methods = {
    // check the passwords on signin
    comparePasswords: function (plainTextPword) {
        return bcrypt.compareSync(plainTextPword, this.password);
    },
    // hash the passwords
    encryptPassword: function (plainTextPword) {
        if (!plainTextPword) {
            return null;
        } else {
            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync(plainTextPword, salt);
            return hash;
        }
    },

    toJSON: function () {
        var user = this.toObject();
        delete user.password;
        return user;
    },


    addCompletedApplication: function (metaData) {
        if (metaData.courseCode && metaData.courseName && metaData.status && metaData.sfId) {
            console.log("INSERTING...");
            this.applications.push({
                courseCode: metaData.courseCode,
                courseName: metaData.courseName,
                status: metatData.status.toLowerCase(),
                sfId: metaData.sfId
            });

            return true;
        }
        return false;
    },

    addIncompleteApplication: function (metaData) {
        if (metaData.courseCode && metaData.courseName && metaData.status && metaData.appRefId) {
            this.applications.push({
                courseCode: metaData.courseCode,
                courseName: metaData.courseName,
                status: metatData.status.toLowerCase(),
                appRefId: metaData.appRefId
            });

            this.save(function (err, user) {
                if (err) {
                    return false;
                }
                console.log(user);
                console.log("ALL SAVED!!");
                return true;
            })

        }
        return false;
    },

    getMyApplications: function () {
        if (this.applications.length > 0) {
            return this.applications;
        }

        return [];
    }
};



//Export model for use within app
module.exports = mongoose.model("user", UserSchema);