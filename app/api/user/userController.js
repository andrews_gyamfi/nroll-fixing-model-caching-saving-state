/**
 * Controller to handle all things relating to the user object.
 * This will also be used in the router. 
 */
var User = require("./userModel"),
    config = require("../../config/config"),
    jwt = require("jsonwebtoken");



/**
 * Method grabs 'id' parameter value, fetches the relevant object and attaches it to the req object for
 * use by other middleware and routing request 
 */
exports.params = function (req, res, next, id) {
    User.findById(id)
        .then(function (user) {
            req.user = user;
            next();
        })
        .catch(function (err) {
            next(err);
        });
}

/**
 * Method finds object by Id and returns it 
 * Simple, it returns the req.user object fetched by the params middleware - above
 */
exports.getOne = function (req, res, next) {
    if (!req.user) next(err)
    res.json(req.user || {});
}

/**
 * Method returns all applicaion objects
 */
exports.getAll = function (req, res, next) {
    if (!req.user) {
        res.status(401);
    }
    return User.find({})
        .then(function (data) {
            res.json(data || {});
        })
        .catch(function (err) {
            next(err);
        })
}

/**
 * Method registers new user
 */
exports.register = function (req, res) {
    if (!req.body.email || !req.body.password) {
        return res.json({
            success: false,
            message: 'Please enter email and password'
        });

    } else {
        var userInfo = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        }

        User.create(userInfo, function (err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'That email address already exists.'
                });
            }
            res.json({
                success: true,
                message: 'Successfully created new user.'
            });
        })
    }
}




/**
 * Method authenticates user
 */
exports.authenticate = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.send({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else {

            if (user.authenticate(req.body.password)) {
                var token = jwt.sign(user, config.secrets.jwt, {
                    expiresIn: 60 // in seconds
                });
                res.json({
                    success: true,
                    token: 'JWT ' + token
                });
            } else {
                console.log("authentication failed!");
                res.send({
                    success: false,
                    message: 'Authentication failed. Passwords did not match.'
                });
            }
        };
    });


}