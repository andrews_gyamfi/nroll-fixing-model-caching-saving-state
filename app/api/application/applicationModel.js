/**
 * This module defines the Schema & model for the application object. 
 * It's recommended that every information on the hardcopy enrolment application form is 
 * recorded here. 
 * NOTE: Pre-Training Review will be abstracted into it's own object and then linked to the relating application
 */

//Require mongoose
var mongoose = require("mongoose");

//Get mongoose Schema
var Schema = mongoose.Schema;

//Create a new Schema for the application form
var applicationSchema = new Schema({
    personalInfo: {
        title: String,
        firstName: String,
        lastName: String,
        previousFirstName: String,
        previousLastName: String,
        dateOfBirth: Date,
        countryOfBirth: String,
        culturalBackground: String,
        yearOfArrival: String,
        gender: String,
        hasPreviousName: Boolean
    },
    citizenship: {
        country: String,
        visaStatus: String,
        otherVisa: {
            subCategory: String,
            expiryDate: Date,
            allowedToStudy: Boolean
        },
        scvVisa: Boolean
    },
    funding: {
        vfh: {
            eligible: Boolean,
            wantsToAccess: Boolean,
            hasRecentProvider: Boolean,
            recentProvider: {
                name: String,
                yearLastEnrolled: String,
                studentNumber: String
            },
            previouslyAccessedVfh: Boolean,
            CHESSN: String
        }
    },
    contactInfo: {
        mobile: String,
        email: String,
        address: {
            same: Boolean,
            residential: {
                street: String,
                suburb: String,
                state: String,
                postcode: String
            },
            postal: {
                street: String,
                suburb: String,
                state: String,
                postcode: String
            }
        }
    },
    emergencyContactInfo: {
        name: String,
        relationship: String,
        number: String
    },
    educationInfo: {
        attendedSchool: Boolean,
        stillAttending: Boolean,
        highestLevel: String,
        attendingYearTwelve: Boolean,
        hasVsn: Boolean,
        vsn: String,
        attendedVicSchool: Boolean,
        recentVicShool: String,
        attendedVet: Boolean,
        recentRtos: {
            one: String,
            two: String,
            three: String
        },
        previousQuals: String,
        previousQualType: String,
        lui: String,
        highSchool: {
            name: String,
            state: String,
            year: String
        }
    },
    employmentInfo: {
        employmentStatus: String,
        occupation: String,
        industry: String
    },
    otherInfo: {
        language: String,
        englishLevel: String,
        studyReason: String,
        otherStudyReason: String,
        isDisabled: Boolean,
        disabilityType: String,
        otherDisabilityType: String,
        hasDisabilityPlan: String,
        disabilityManagementPlan: String,
        hasUsi: Boolean,
        usi: String,
        rpl: Boolean,
        ct: Boolean
    },
    courseChoices: [],
    declared: Boolean,
    files: [],
    // User who owns this application
    user: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: {
        type: Date,
        default: new Date()
    }
})


applicationSchema.methods = {
    getFirstName: function () {
        return this.personalInfo.firstName;
    },
    getLastName: function () {
        return this.personalInfo.lastName;
    }
}

//Export model for use within app
module.exports = mongoose.model("application", applicationSchema);