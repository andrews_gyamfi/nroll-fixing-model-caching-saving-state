/**
 * This route handles all requests relating to an hire application
 */

var express = require("express"),
    HireRouter = express.Router(),
    salesforce = require("../salesforce")();


/**
 * Middleware to invoke form validator
 */

HireRouter.route("/")
    .get(function (req, res) {
        res.render("hire");
    })
    .post(function (req, res) {
        var hiredItem = req.body.hireItem,
            applicationId = req.body.id,
            it = false,
            sms = false,
            idm = false;

        switch (hiredItem) {
        case "it":
            it = true;
            break;
        case "idm":
            idm = true;
            break;
        case "sms":
            sms = true;
            break;
        default:
            console.log("Invalid value provide");
        }


        salesforce.createHireAgreement({
            Application__c: applicationId,
            Dip_IDM_Essential_Hardware__c: idm,
            Dip_IT_Essential_Hardware__c: it,
            Diploma_of_Specialist_Makeup_Services__c: sms
        }, function (id) {
            console.log(id);
        })
        res.json({
            succces: true,
        });

    });

module.exports = HireRouter;