/**
 * CourseFactory handles 
 * Function: 
 * 1. It pulls courses from API
 * 2. Stores courses in array/cache for later use by app
 */

angular.module("nroll")

/* Requires: $http service to function properly */
.factory("CourseFactory", ["$http", "$q", function ($http, $q) {
    /**
     * Method grabs all courses from API
     */
    var getCourses = function () {
        return $http.get("api/courses/", {
                cache: true
            })
            .then(function (response) {
                return response.data.data;
            })

    }

    /**
     * Method returns grabs all intakes 
     */
    var getIntakes = function () {
        return $http.get("api/courses/intakes", {
                cache: true
            })
            .then(function (response) {
                return response.data.data;
            })
    }
    return {
        getCourses: getCourses,
        getIntakes: getIntakes
    };

}]);