/**
 * TokenFactory is responsible for managing tokens received from backend API.
 * It's responsible for: 
 * 1. Creating tokens on user register and login states 
 * 2. Deleting tokens on user logout 
 */
angular.module("nroll")

/* Requires $window object to store token in localStorage */
.factory("TokenFactory", ["$window", function ($window) {

    /* Declare required variables */
    var cachedToken,
        storage = $window.localStorage;

    /**
     * Method stores token in local storage IF TOKEN IS PASSED or
     * resets it to null IF NO TOKEN IS PASSED 
     */
    var setToken = function (token) {
        if (token) {
            cachedToken = token;
            storage.setItem("token", token);
        } else {
            storage.removeItem("token");
        }
    }

    /**
     * Method returns stored token 
     * If cachedToken has no value, method attempts  to fetch token from localStorage 
     * and assigns the value to cacheToken. 
     * @returns cachedToken
     */
    var getToken = function () {
        if (!cachedToken) {
            cachedToken = storage.getItem("token");
            return cachedToken;
        }

        return cachedToken;
    }

    /* Expose setToken & getToken methods via module-revealing pattern */
    return {
        setToken: setToken,
        getToken: getToken

    }
  }]);