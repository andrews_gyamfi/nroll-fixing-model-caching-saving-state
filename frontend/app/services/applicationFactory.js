/**
 * ApplicationFactory models the application form
 */
angular.module("nroll")

.factory("Applicant", ["$http", "$q", function ($http, $q) {
    /* Declare Applicant's personal information model */

    function Applicant() {

        this.personalInfo = {
            title: "",
            firstName: "",
            lastName: "",
            previousFirstName: "",
            previousLastName: "",
            dateOfBirth: "",
            countryOfBirth: "",
            culturalBackground: "",
            yearOfArrival: "",
            gender: "",
            hasPreviousName: null
        };

        /* Citizenship status */
        this.citizenship = {
            country: "",
            visaStatus: null,
            otherVisa: {
                subCategory: "",
                expiryDate: "",
                allowedToStudy: null
            },
            scvVisa: null
        }

        /* Course Funding */
        this.funding = {
            vfh: {
                eligible: null,
                wantsToAccess: null,
                hasRecentProvider: null,
                recentProvider: {
                    name: "",
                    yearLastEnrolled: "",
                    studentNumber: ""
                },
                previouslyAccessedVfh: null,
                CHESSN: ""
            }
        }

        /* Declare Applicant's contact information model */
        this.contactInfo = {
            mobile: "",
            email: "",
            address: {
                same: true,
                residential: {
                    street: "",
                    suburb: "",
                    state: "",
                    postcode: ""
                },
                postal: {
                    street: "",
                    suburb: "",
                    state: "",
                    postcode: ""
                }
            }
        };

        /* Declare emergency contact model */
        this.emergencyContactInfo = {
            name: "",
            relationship: "",
            number: ""
        };

        /* Declare education background model */
        this.educationInfo = {
            attendedSchool: null,
            stillAttending: null,
            highestLevel: "",
            attendingYearTwelve: null,
            hasVsn: null,
            vsn: "",
            attendedVicSchool: null,
            recentVicShool: "",
            attendedVet: null,
            recentRtos: {
                one: "",
                two: "",
                three: ""
            },
            previousQuals: "",
            previousQualType: "",
            lui: "",
            highSchool: {
                name: "",
                state: "",
                year: ""
            }
        };

        /* Declare employment history model */
        this.employmentInfo = {
            employmentStatus: "",
            occupation: "",
            industry: ""
        }

        /* Declare other details model */
        this.otherInfo = {
            language: "",
            englishLevel: "",
            studyReason: "",
            otherStudyReason: "",
            isDisabled: null,
            disabilityType: "",
            otherDisabilityType: "",
            hasDisabilityPlan: "",
            disabilityManagementPlan: "",
            hasUsi: null,
            usi: "",
            rpl: null,
            ct: null
        }

        /* Applicant Course details */
        this.courseInfo = {
            courseId: "",
            intakeId: ""
        }

        /* selected courses */
        this.courseChoices = [];

        this.declared;

        /* Array of files */
        this.files = [];

    }

    /**
    /* Method takes applicant's infor and sends it to API backend */
    Applicant.prototype.getInfo = function () {

        var appInfo = {
            personal: this.personalInfo,
            contact: this.contactInfo,
            emergency: this.emergencyContactInfo,
            educational: this.educationInfo,
            employment: this.employmentInfo,
            other: this.otherInfo,
            courseChoices: this.courseChoices,
            citizenship: this.citizenship,
            funding: this.funding,
            declared: this.declared
        };

        return appInfo;

    }

    /**
     * Method returns true if number of course choices has been reached
     */
    Applicant.prototype.courseLimitReached = function () {
        return this.courseChoices.length === 1;
    }

    /**
     * Method returns length of course choices array 
     */
    Applicant.prototype.getChoicesLength = function () {
        return this.courseChoices.length;
    }

    /**
     * Method checks if user has selected at least one course 
     */
    Applicant.prototype.hasChoices = function () {
        return !(this.courseChoices.length <= 0)
    }

    /**
     * Method deletes object instance 
     */
    Applicant.prototype.destroy

    return Applicant;


}]);