/**
 * Application Manager
 * Primary responsible for managing applications as the name suggests.
 * Functions include: 
 * 1. Retrieving collections of application from API
 * 2. Storing and manipulation collections of applications - if any
 * 3. Persisting current application in-progress to localStorage and retrieving on application refresh
 * 4. Handling wizard steps validity
 */

angular.module("nroll")

.factory("EnrolmentManager", ["Applicant", "$localStorage", "$http", "$rootScope", function (Applicant, $localStorage, $http, $rootScope) {

    /* Enrolment managere handles enrolment applications */
    var enrolmentManager = {
        /* Array list of applications */
        applications: [],

        appId: $rootScope.currentUser.firstName + "CurrentApp",
        /* Holds current instance */
        currentApplication: $localStorage[this.appId] || null,

        /* Flag to track application in progress tracker */
        newApplicationInProgress: false,

        /**
         * Method creates new application and adds it to collection with an ID
         * @return object ID - method returns id of newly created application object
         */
        getApplication: function (id) {

            /* Look for object if it already exists */
            var objInstance;

            if ($localStorage.hasOwnProperty(this.appId) && $localStorage[this.appId] !== null) {

                /* It means there's an application already in progress so return it */
                this.currentApplication = this.getCurrentApplication();

            } else {
                this.currentApplication = new Applicant();
            }

            /* Return instance */
            return this.currentApplication;

        },

        /* Method removes application instance it it exists in array */
        removeApplication: function (id) {
            var objInstance = this.applications[id];
            if (objInstance) {
                delete this.applications[id]
            }

            this.newApplicationInProgress = false;

            return objInstance;
        },

        /* Checks if there's already an application in progress */
        applicationInProgress: function () {
            return this.newApplicationInProgress;
        },

        /* Get current application instance */
        getCurrentApplication: function () {
            return angular.extend(new Applicant, $localStorage[this.appId]);
        },

        /* Persist current application in local storage -- to main state on refresh */
        serializeCurrentApplication: function () {
            /* Set application in progress to true */
            this.newApplicationInProgress = true;

            $localStorage[this.appId] = this.currentApplication;
        },

        /* Remove persisted object */
        destroyCurrentApplication: function () {
            if ($localStorage.hasOwnProperty(this.appId)) {
                delete $localStorage[this.appId];
                this.currentApplication = null;
            }
        },
        /* Send info to API */
        apply: function () {
            var info = this.getCurrentApplication().getInfo();
            return $http.post("/api/application", info);

        },

        save: function () {
            var jsonString = JSON.stringify(this.getCurrentApplication().getInfo());
            return $http.post("/api/application/save", jsonString);

        },

        resume: function (appId) {
            return $http.get("/api/application/" + appId);
        }
    }

    /* Return enrolment manager */
    return enrolmentManager;

}])