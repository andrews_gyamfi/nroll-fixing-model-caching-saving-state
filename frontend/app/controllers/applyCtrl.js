angular.module("nroll")
    .controller("applyCtrl", ["$rootScope", "EnrolmentManager", "referenceData", "countries", "languages", "$uibModal", function ($rootScope, EnrolmentManager, referenceData, countries, languages, $uibModal) {
        /* Declare view-model variable and assign it to this scope */
        var vm = this;

        /* Panels */
        vm.panels = {
            personalDetails: true,
            contactDetails: true,
            educationDetails: true,
            otherDetails: true
        }

        /* Assign Applicant service to local scope variable */
        vm.applicant = EnrolmentManager.getCurrentApplication();

        /* Set email address to that of current user */
        vm.applicant.contactInfo.email = $rootScope.currentUser.email;

        /* Assign referenceData */
        vm.references = referenceData.refData;

        /* Assign countries to variable */
        vm.countries = countries;

        /* Assign languages to variable */
        vm.languages = languages;

        /* Method returns whether school state is VIC or not */
        var schoolStateIsVic = function () {
            return vm.applicant.educationInfo.highSchool.state === "VIC";
        }

        /* Method returns whether student has VSN */
        var hasVsn = function () {
            return vm.applicant.educationInfo.hasVsn
        }

        /* Method returns boolean: depending on hasVsn value */
        var noVsn = function () {
            if (vm.applicant.educationInfo.hasVsn === null) {
                return false;
            };

            if (!vm.applicant.educationInfo.hasVsn && vm.applicant.educationInfo.highSchool.state === "VIC") {
                return true;
            };
        }


        /* Method returns boolean: depending on whether student attended VET school */
        var attendedVet = function () {
            return vm.applicant.educationInfo.attendedVet && !hasVsn();
        }

        /* Method returns boolean: depending on whether student attended Vic school */
        var attendedVicSchool = function () {
            return vm.applicant.educationInfo.highSchool.state === "VIC" && vm.applicant.educationInfo.attendedVicSchool;
        }


        /* Method returns boolean: depending on selected employment status */
        var isEmployed = function () {
            return vm.applicant.employmentInfo.employmentStatus === "Full time employee" || vm.applicant.employmentInfo.employmentStatus === "Part time employee";

        }

        /* Method returns boolean: depending on whether applicant select other disability */
        var hasOtherDisability = function () {
            return vm.applicant.otherInfo.disabilityType === "Other";
        }

        /* Method returns boolean: depending on whether applicant selected has disability management plan or not */
        var hasDisabilityPlan = function () {
            return vm.applicant.otherInfo.hasDisabilityPlan;
        }

        /* Method collapses accordion */
        var toggleCollapse = function (panel) {
            panel = !panel;
        }

        /* Method returns boolean based on language spoken*/
        var languageNotEnglish = function () {
            if (vm.applicant.otherInfo.language === "") {
                return false;
            }

            return vm.applicant.otherInfo.language !== "English";
        }

        /* Method returns boolean if student lives in QLD */
        var livesInQld = function () {
            return vm.applicant.contactInfo.address.residential.state === 'QLD';
        }


        /* Method returns boolean if student is still attending secondary school */
        var stillAttendingSec = function () {
            return vm.applicant.educationInfo.stillAttending;
        }

        /* Method returns boolean if student is still attending year twelve*/
        var attendingYearTwelve = function () {
            return vm.applicant.educationInfo.attendingYearTwelve;
        }

        /* Method returns boolean if student has disability */
        var hasDisability = function () {
            return vm.applicant.otherInfo.isDisabled;
        }

        var attendedSecondarySchool = function () {
            if (vm.applicant.educationInfo.highestLevel === '') {
                return false;
            }

            if (vm.applicant.educationInfo.highestLevel !== 'Never Attended School') {
                return true;
            }

            return false;
        }

        /* Method returns boolean if student is not born in Australia */
        var notBornInAustralia = function () {
            if (vm.applicant.personalInfo.countryOfBirth === "") {
                return false;
            }


            if (vm.applicant.personalInfo.countryOfBirth !== 'Australia') {
                return true;
            }

            return false;
        }

        /* Method returns boolean depending on whether student has previous qualification or not*/
        var hasPreviousQual = function () {
            if (vm.applicant.educationInfo.previousQuals !== "") {
                return true;
            }
            return false;
        }

        /* Cancellation confirmation modal. Will move this into a modal service on its own */
        var cancellationModal = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: "../../views/modals/cancelEnrolment.html",
                controller: "choicesCtrl"
            });
        };


        /* Expose methods */
        vm.schoolStateIsVic = schoolStateIsVic;
        vm.hasVsn = hasVsn;
        vm.attendedVet = attendedVet;
        vm.attendedVicSchool = attendedVicSchool;
        vm.noVsn = noVsn;
        vm.isEmployed = isEmployed;
        vm.hasOtherDisability = hasOtherDisability;
        vm.hasDisabilityPlan = hasDisabilityPlan;
        vm.toggleCollapse = toggleCollapse;
        vm.languageNotEnglish = languageNotEnglish;
        vm.livesInQld = livesInQld;
        vm.stillAttendingSec = stillAttendingSec;
        vm.attendingYearTwelve = attendingYearTwelve;
        vm.hasDisability = hasDisability;
        vm.attendedSecondarySchool = attendedSecondarySchool;
        vm.notBornInAustralia = notBornInAustralia;
        vm.cancellationModal = cancellationModal;
        vm.hasPreviousQual = hasPreviousQual;


}]);