angular.module("nroll")
    .controller("uploadCtrl", ["Upload", "EnrolmentManager", "$uibModal", function (Upload, EnrolmentManager, $uibModal) {
        /* Declare view-model variable and assign it to this scope */
        var vm = this;

        vm.applicant = EnrolmentManager.getCurrentApplication();
        /* Reference to Applicant object */
        vm.files = vm.applicant.files;

        /* Add selected file to list of files to be uploaded */
        var addFile = function (file) {

            /* Fix for Upload bug where a null object is returned before file is even selected*/
            if (file === null) {
                return;
            } else {
                /* add files to array of files - these will be uploaded on completion of the form*/
                vm.applicant.files.push(file);
            }
        };

        /* Method removes selected file */
        var removeFile = function (index) {
            _.pullAt(vm.files, index);
        }


        /* Method uploads files */
        var uploadFiles = function () {
            Upload.upload({
                url: "api/upload/a0P280000035iNw",
                data: {
                    files: vm.files
                }
            });
        }

        /* Cancellation confirmation modal. Will move this into a modal service on its own */
        var cancellationModal = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: "../../views/modals/cancelEnrolment.html",
                controller: "choicesCtrl"
            });
        };


        /* Expose functions */
        vm.removeFile = removeFile;
        vm.uploadFiles = uploadFiles;
        vm.cancellationModal = cancellationModal;
        vm.addFile = addFile;



    }]);