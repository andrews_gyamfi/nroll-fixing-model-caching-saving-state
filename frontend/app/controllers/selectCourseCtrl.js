/**
 * Module handles course selection
 */

angular.module("nroll")

/* Requires: User model to function properly */

.controller("selectCourseCtrl", ["$scope", "EnrolmentManager", "intakes", "countries", "referenceData", "$uibModal", function ($scope, EnrolmentManager, intakes, countries, referenceData, $uibModal) {

    /* Set view model variable */
    var vm = this;

    /* Create new enrolment manager */
    vm.enrolmentManager = EnrolmentManager;

    /* Get reference to current application instance */
    vm.applicant = vm.enrolmentManager.getApplication();

    /* Set modal options */
    vm.modalOptions = {
        animation: true,
        templateUrl: "../../views/modals/cancelEnrolment",
        controller: "choicesCtrl"
    }

    /* Assign intakes to model */
    vm.intakes = intakes;

    /* Assign Reference Data */
    vm.references = referenceData.refData;
    /* Assign countries to model*/
    vm.countries = countries;

    /* Assign extracted courses to mdoel */
    vm.courses = [];

    /* course choices */
    vm.courseChoices = vm.applicant.courseChoices;

    /* Filtered courses - all duplicates removed */
    vm.filteredCourses = [];

    /* Unique courses */
    vm.uniqueCourses;

    /* Course choices */

    /* Intake relevant to chosen course */
    vm.currentIntakes = [];

    /* Campuses relevant to chosen course */
    vm.currentCampuses = [];

    /* Maximum courses allowed */
    vm.maxAllowedCourses = 1;

    /* Maximum courses reached flag */
    vm.maxAllowedReached = vm.applicant.courseLimitReached();

    /* Flag for choices - to be used to show/hide elements on page */
    vm.hasChoices = vm.applicant.hasChoices();

    /**
     * Method adds a course to the course choices array
     */
    var addCourseChoice = function () {
        if (vm.courseChoices.length < vm.maxAllowedCourses) {
            vm.courseChoices.push({
                course: vm.courseChoices.course,
                campus: vm.courseChoices.campus,
                intake: vm.courseChoices.intake
            });

            /* Set has choices flag */
            if (!vm.hasChoices) {
                vm.hasChoices = true;
            }
        }

        if (vm.courseChoices.length === 1) {
            vm.maxAllowedReached = true;
        }

    }

    /**
     * Method removes course from course choices array
     */
    var removeCourseChoice = function (index) {
        _.pullAt(vm.courseChoices, index);

        if (vm.courseChoices.length < vm.maxAllowedCourses) {
            vm.hasChoices = false;
        }

        vm.maxAllowedReached = false;
    }


    /**
     * Method extract courses from massive intakes object
     */
    var extractCourses = function (intakes) {
        _.filter(intakes, function (intake) {
            vm.courses.push({
                id: intake.Course__r.Id,
                code: intake.Course__r.ProductCode,
                name: intake.Course__r.Name
            });
        });
    }

    /* invoke function */
    extractCourses(vm.intakes);

    /**
     * Method filters courses by removing duplicates
     */
    var filterCourses = function () {
        vm.filteredCourses = _.uniqBy(vm.courses, function (intake) {
            return intake.code;
        });
    }

    /* invoke function to filter courses */
    filterCourses();

    /**
     * Method filters campuses by removing duplcates 
     */
    var filterCampuses = function () {
        vm.currentCampuses = _.uniqBy(vm.currentIntakes, function (intake) {
            return intake.Campus__r.Name;
        });
    }

    /* Get All intake dates */
    var getIntakes = function (course) {

        if (vm.currentIntakes.length > 0) {
            vm.currentIntakes = [];
        }
        _.filter(vm.intakes, function (intake) {
            if (intake.Course__r.Id === course.id) {
                vm.currentIntakes.push(intake);
            }
        });

        filterCampuses();

    }

    /* Method invokes modal to confirm cancellation action */
    var cancelModal = function () {

    }


    /**
     * Method returns TRUE if study reason is 'Other'
     */
    var showOtherReason = function () {
        return vm.applicant.otherInfo.studyReason === "Other"
    }

    /**
     * Method to check if visa status should be displayed
     */
    var showVisaStatus = function () {
        if (vm.applicant.citizenship.country === null || vm.applicant.citizenship.country === "") {
            return false;
        }

        if (vm.applicant.citizenship.country === "New Zealand" && vm.applicant.citizenship.scvVisa === null) {
            return false;
        }


        if (vm.applicant.citizenship.country === "New Zealand" && !vm.applicant.citizenship.scvVisa) {
            return true;
        }


        if (vm.applicant.citizenship.country === "Australia" || vm.applicant.citizenship.country === "New Zealand") {
            return false;
        }


        return true;
    }

    /**
     * Method to reveal SCV visa section if New Zealand citizen
     */
    var showScvVisa = function () {
        return vm.applicant.citizenship.country === "New Zealand";
    }

    /**
     * Method returns boolean if New Zealand citizen has SCV Visa or not
     */
    var hasScvVisa = function () {
        return vm.applicant.citizenship.scvVisa;
    }


    /* Method determines VFH eligibility */
    var vfhEligible = function () {
        if (vm.applicant.citizenship.country === "Australia") {
            return true;
        }

        if (vm.applicant.citizenship.country === "New Zealand" && hasScvVisa()) {
            return true;
        }

        if ((vm.applicant.citizenship.country !== "Australia" || vm.applicant.citizenship.country !== "New Zealand") && vm.applicant.citizenship.visaStatus === "Permanent Humanitarian Visa Holder") {
            return true;
        }

        return false;
    }


    /**
     * Method checks to see if other visa section should be displayed
     */

    var showOtherVisa = function () {

        if (vm.applicant.citizenship.country === "New Zealand" && !vm.applicant.citizenship.scvVisa && vm.applicant.citizenship.visaStatus === 'Other Visa') {
            return true;
        }


        if (vm.applicant.citizenship.country === "Australia" || vm.applicant.citizenship.country === "New Zealand") {
            return false;
        }

        if (vm.applicant.citizenship.visaStatus === 'Other Visa') {
            return true;
        }
        return false;
    }


    /* Method returns boolean depending on whether student is accessing VFH or not */
    var accesingVfh = function () {
        return vm.applicant.funding.vfh.wantsToAccess;
    }

    /* Method returns boolean depending on whether student has prior education with a Higher Education Provider or RTO */
    var hasPriorTraining = function () {
        return vm.applicant.funding.vfh.hasRecentProvider;
    }

    /* Method returns boolean: depending on whether applicant has accessed VFH previously */
    var hasAccessedVfhBefore = function () {
        return vm.applicant.funding.vfh.previouslyAccessedVfh;
    }


    /* Cancellation modal */
    var cancellationModal = function () {
        $uibModal.open({
            animation: true,
            backdrop: 'static',
            templateUrl: "../../views/modals/cancelEnrolment.html",
            controller: "choicesCtrl"
        });
    };

    /* Method checks to make sure at least course has been selected before proceeding 
    if (vm.applicant.getChoicesLength !== 1) {
        vm.coursesForm.$setValidity(false);
    }
    */

    /* Persist object in localStorage */
    var persist = function () {
        vm.enrolmentManager.serializeCurrentApplication();
    }


    /* Expose methods on view-model variable */
    vm.addCourseChoice = addCourseChoice;
    vm.removeCourseChoice = removeCourseChoice;
    vm.getIntakes = getIntakes;
    vm.showVisaStatus = showVisaStatus;
    vm.showOtherVisa = showOtherVisa;
    vm.showOtherReason = showOtherReason;
    vm.showScvVisa = showScvVisa;
    vm.hasScvVisa = hasScvVisa;
    vm.vfhEligible = vfhEligible;
    vm.accesingVfh = accesingVfh;
    vm.hasPriorTraining = hasPriorTraining;
    vm.hasAccessedVfhBefore = hasAccessedVfhBefore;
    vm.cancelEnrollment = cancellationModal;
    vm.persist = persist;


    $scope.$watch("coursesForm", function (coursesForm) {
        if (coursesForm) {
            console.log(coursesForm);
        }
    })

}]);