/**
 * Header Controller
 * Responsible for showing and hiding relevant navigation links 
 * based on user's login status
 */
angular.module("nroll")

/* Requires AuthFactory */
.controller("choicesCtrl", ["$state", "$scope", "$uibModalInstance", "EnrolmentManager", function ($state, $scope, $uibModalInstance, EnrolmentManager) {

    $scope.enrolmentManager = EnrolmentManager;

    $scope.ok = function () {
        $scope.enrolmentManager.destroyCurrentApplication();
        $uibModalInstance.close();
        $state.go("app.home");

    };

    $scope.save = function () {
        $scope.enrolmentManager.save()
            .then(function (response) {
                if (response.status === 200 && response.data.succces) {
                    $scope.enrolmentManager.destroyCurrentApplication();
                    $uibModalInstance.close();
                }
            })
            .catch(function (response) {

            })
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.dashboard = function () {
        $uibModalInstance.close();
        $state.go("app.home");
    }

}]);