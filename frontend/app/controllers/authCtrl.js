/**
 * Authentication controller handles frontend user authentication
 * Handles: 
 * 1. Login
 * 2. Registration
 * 3. Tokenizing 
 */

angular.module("nroll")

.controller("authCtrl", ["$rootScope", "$scope", "$state", "AuthFactory", "TokenFactory", "User", "$auth", function ($rootScope, $scope, $state, AuthFactory, TokenFactory, $auth) {
    /**
     * Mock reg model
     */
    $scope.regUser = {
        firstName: "",
        lastName: "",
        email: "",
        password: ""
    };

    /**
     * Mock login User model
     */
    $scope.logUser = {
        email: "",
        password: ""
    };


    /**
     * Method handles user registration
     * Takes input and queries API
     */
    $scope.register = function () {
        AuthFactory.register($scope.regUser)
            .then(function (response) {
                if (response.data.success) {
                    TokenFactory.setToken(response.data.token);
                    $rootScope.$broadcast("isLoggedIn", response.data.user.firstName);
                    $state.go("home");
                    User.isLoggedIn = true;
                    User.setUser({
                        firstName: response.data.user.firstName,
                        lastName: response.data.user.lastName,
                        email: response.data.user.email
                    });


                }
            })
            .catch(function (response) {
                console.log(response);
            });
    }

    /**
     * Method handles user login: 
     * Takes input and queries API
     */
    $scope.login = function () {
        console.log("Trying to log in");
        AuthFactory.login($scope.logUser.email, $scope.logUser.password)
            .then(function (response) {
                if (response.data.success) {
                    TokenFactory.setToken(response.data.token);;
                    $rootScope.$broadcast("user:isLoggedIn", {
                        userName: response.data.user.firstName
                    });
                    $state.go("home");
                    User.isLoggedIn = true;
                    User.setUser({
                        firstName: response.data.user.firstName,
                        lastName: response.data.user.lastName,
                        email: response.data.user.email
                    });
                }
            })
            .catch(function (response) {
                console.log(response);
            })
    }

    /**
     * Method STORES token and EMITS 
     * loggedin event for header controller 
     */
    function storeToken(token) {
        TokenFactory.setToken(token);
        $rootScope.$broadcast("user:isLoggedIn", {
            userName: response.data.user.firstName
        });

    }

    /**
     * Method redirects to main dashboard - home
     */
    function goHome() {
        $state.go("home");
    }


    /**
     * Error handler for asynchronous promise
     */
    function errorHandler() {
        console.log("Could not retriev token");
    }


}]);