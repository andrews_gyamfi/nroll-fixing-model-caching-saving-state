/**
 * Dashboard Controller
 */
angular.module("nroll")

.controller("dashboardCtrl", ["User", "EnrolmentManager", "myapplications", function (User, EnrolmentManager, myapplications) {

    var vm = this;

    /* Welcome Message */
    vm.welcomeMessage = "You currently do not  have any applications.";

    /* Set if applicant has applications in progress */
    vm.myapplications = myapplications;


    /* Boolean flag - has application */
    vm.hasApplications = Boolean(myapplications.length) ? true : false;
    //
    //    CourseFactory.getCourses()
    //        .then(function (response) {
    //            console.log(response);
    //            vm.courses = response;
    //        })

    vm.resumeApp = function (index) {
        var appId = vm.myapplications[index].appRefId
        EnrolmentManager.resume(appId)
            .then(function (response) {
                console.log("success");
                console.log(response);
            })
            .catch(function (response) {
                console.log(response);
            })
    }
}]);