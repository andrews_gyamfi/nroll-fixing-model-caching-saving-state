angular.module("nroll")
    .controller("declarationCtrl", ["Upload", "Applicant", "$uibModal", "EnrolmentManager", function (Upload, Applicant, $uibModal, EnrolmentManager) {
        /* Declare view-model variable and assign it to this scope */
        var vm = this;

        /* Reference to enrolment manager object */
        vm.enrolmentManager = EnrolmentManager;

        /* Controller reference to applicant object */

        vm.applicant = EnrolmentManager.getCurrentApplication();

        /* Apply button disable state */
        vm.buttonDisable = false;

        /* Cancellation confirmation modal. Will move this into a modal service on its own */
        var cancellationModal = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: "../../views/modals/cancelEnrolment.html",
                controller: "choicesCtrl"
            });
        };

        var successModal = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: "../../views/modals/success.html",
                controller: "choicesCtrl"
            });
        };

        var errorModal = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: "../../views/modals/errorInfo.html",
                controller: "choicesCtrl"
            });
        };

        /* Final submit function */
        var submit = function () {

            /* Disable the apply button */
            vm.buttonDisable = true;

            /* Attempt sending application details to API */
            vm.enrolmentManager.apply()
                .then(function (response) {
                    if (response.status === 200 || response.data.success) {

                        /* If application details is successful, attempt document uploads */
                        Upload.upload({
                            url: "api/upload/" + response.data.id,
                            data: {
                                files: vm.applicant.files
                            }

                        }).then(function (response) {
                            if (response.status === 200 || response.data.success) {
                                /* Show success Modal */
                                successModal()
                                vm.enrolmentManager.destroyCurrentApplication();
                            };
                        });
                    }
                }).catch(function (response) {
                    if (response.status === 400 || !resonse.data.succces) {
                        errorModal();
                    }
                    /* If wasn't successful, enable the apply button again */
                    vm.buttonDisable = false;
                })
        };

        /* Expose functions */
        vm.cancellationModal = cancellationModal;
        vm.submit = submit;

    }]);