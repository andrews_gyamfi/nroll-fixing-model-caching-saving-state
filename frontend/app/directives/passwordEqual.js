angular.module("nroll")

.directive("passwordEquals", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: {
            otherPassword: "="
        },
        link: function (scope, elem, attr, ngModelCtrl) {
            ngModelCtrl.$validators.confirmPassword = function (modelValue) {
                return (modelValue === scope.otherPassword);
            }

            scope.$watch("otherPassword", function () {
                ngModelCtrl.$validate();
            })
        }
    }

})