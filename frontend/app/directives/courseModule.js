angular.module("nroll")

.directive("courseModule", function () {
    return {
        restrict: "EA",
        templateUrl: "../../views/directives/courseModule.html"
    }
});