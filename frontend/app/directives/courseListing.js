angular.module("nroll")
    .directive("courseListing", function () {

        /**
         * Controller 
         */
        var courseCtrl = function ($scope, CourseFactory, $state) {


            /* Declare vm variable */
            var vm = this;

            console.log(vm.sendDetails);
            /* Declare directive variables */
            vm.campuses = [];
            vm.courses = [];
            vm.intakes = [];
            vm.filteredCourses;
            vm.filteredCampuses;
            vm.currentCampuses = [];
            vm.currentIntakes = [];
            vm.courseIndex = 0;
            vm.stepValid = false;
            vm.startDateChosen;
            vm.campusChosen;
            vm.courseChosen;

            /**
             * Method grabs course info from intakes 
             */

            $scope.$watchGroup(["startDateChosen", "campusChosen", "courseChosen"], function (oldValue, newValue) {
                console.log(oldValue);
                console.log(newValue);
            })

            var grabCourses = function (intakes) {
                _.filter(intakes, function (intake) {
                    vm.courses.push({
                        id: intake.Course__r.Id,
                        code: intake.Course__r.ProductCode,
                        name: intake.Course__r.Name,
                        industry: intake.Course__r.Industry__c
                    });
                });
            }

            /**
             * Method filters courses by removing duplicates
             */
            var filterCourses = function () {
                vm.filteredCourses = _.uniqBy(vm.courses, function (intake) {
                    return intake.code;
                });
            }

            /**
             * Method filters campuses by removing duplcates 
             */

            var filterCampuses = function () {
                vm.currentCampuses = _.uniqBy(vm.currentIntakes, function (intake) {
                    return intake.campus;
                });

            }

            /**
             * Method filters intakes 
             */
            var filterIntakes = function (intakes) {
                _.filter(intakes, function (intake) {
                    vm.intakes.push({
                        id: intake.Id,
                        name: intake.Name,
                        startDate: intake.Online_Orientation_Day__c,
                        courseCode: intake.Course__r.ProductCode,
                        campus: intake.Campus__r.Name
                    });
                });
            }

            /* Get All intake dates */
            var getIntakes = function (course, index) {

                vm.courseIndex = index;

                vm.courseChosen = course;

                /* Variable to hold specific intake */
                if (vm.currentIntakes.length > 0) {
                    vm.currentIntakes = [];
                }
                _.filter(vm.intakes, function (intake) {
                    if (intake.courseCode === course.code) {
                        vm.currentIntakes.push(intake);
                    }
                });

                /* Filter intake campuses */
                filterCampuses();
            }


            /* Function sends details to parent scope */
            var sendInfo = function () {
                vm.sendDetails()({
                    course: vm.courseChosen.id,
                    campus: vm.campusChosen.id,
                    intake: vm.startDateChosen.id
                });

                $state.go("app.apply.completeDetails");
            }


            /** 
             * Get intakes from API
             */
            CourseFactory.getIntakes()
                .then(function (intakes) {

                    vm.campuses = _.map(intakes, function (intake) {
                        return intake.Campus__r.Name;
                    })

                    /* Get courses from array of intakes */
                    grabCourses(intakes);

                    /* Filter courses to remove duplicates */
                    filterCourses();


                    /* Filter intakes */
                    filterIntakes(intakes);

                });

            vm.getIntakes = getIntakes;
            vm.sendInfo = sendInfo;
        }


        return {
            restrict: "EA",
            templateUrl: "../../views/directives/course.html",
            scope: {
                sendDetails: "&"
            },
            controller: courseCtrl,
            controllerAs: "ctrl",
            bindToController: true
        }
    });