/**
 * Custom validation directive specifically for regex
 */
angular.module("nroll")
    /* Directive declaration */
    .directive("regexValidate", function () {
        var regexTypes = {
                mobile: /^\(?(?:\+?61|0)(?:(?:2\)?[ -]?(?:3[ -]?[38]|[46-9][ -]?[0-9]|5[ -]?[0-35-9])|3\)?(?:4[ -]?[0-57-9]|[57-9][ -]?[0-9]|6[ -]?[1-67])|7\)?[ -]?(?:[2-4][ -]?[0-9]|5[ -]?[2-7]|7[ -]?6)|8\)?[ -]?(?:5[ -]?[1-4]|6[ -]?[0-8]|[7-9][ -]?[0-9]))(?:[ -]?[0-9]){6}|4\)?[ -]?(?:(?:[01][ -]?[0-9]|2[ -]?[0-57-9]|3[ -]?[1-9]|4[ -]?[7-9]|5[ -]?[018])[ -]?[0-9]|3[ -]?0[ -]?[0-5])(?:[ -]?[0-9]){5})$/,
                year: /(?:(?:19|20)[0-9]{2})/,
                postcode: /^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/,
                usi: /^([A-Za-z0-9]){10}$/,
                lui: /^([0-9]){10}$/,
                chessn: /^([0-9]){10}$/,
                stringOnly: /^([A-Za-z])+$/,
                stringWithSpaces: /^[A-Za-z\s]+$/,
                streetAddress: /^[\d\w\s//]+$/,
                postalAddress: /^[A-Za-z\w\s///.]+$/
            }
            /** 
             * Link function definition 
             */
        var link = function (scope, elem, attr, ngModel) {
            /* Declare variable to hold validation type */

            /* Get the validation type from element */
            var validationType = attr.validationType,
                regex = new RegExp(regexTypes[validationType]);

            /* Declare and attach new validation function */
            ngModel.$validators.regexValidate = function (modelValue, viewValue) {
                var value = modelValue || viewValue;

                /* If field is not required, do not validate */
                attr.$observe("required", function (value) {
                    if (!value) {
                        ngModel.$setValidity("regexValidate", true);
                    }
                })

                /* return boolean result */
                return regex.test(value);
            }

        }

        /* Return directive definition object */
        return {
            restrict: "A",
            require: "ngModel",
            scope: {},
            link: link
        }
    })