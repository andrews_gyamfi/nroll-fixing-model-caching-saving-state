angular.module("nroll")

.directive('routeLoadingIndicator', function ($rootScope) {
    return {
        restrict: 'E',
        template: "<div ng-show='isRouteLoading' class='loading-indicator'>" +
            "<div class='loading-indicator-body'>" +
            "<div class='spinner'><wandering-cubes-spinner></wandering-cubes-spinner></div>" +
            "</div>" +
            "</div>",
        replace: true,
        link: function (scope, elem, attrs) {
            scope.isRouteLoading = false;

            $rootScope.$on('$stateChangeStart', function () {
                scope.isRouteLoading = true;
            });
            $rootScope.$on('$stateChangeSuccess', function () {
                scope.isRouteLoading = false;
            });
        }
    };
});