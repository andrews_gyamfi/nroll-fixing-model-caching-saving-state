/**
 * User handles all things user
 * 1. It provides basic user info for app UI
 * 2. It exposes accessor and setter methods for user data
 * 3. It's responsible for user registration
 */
angular.module("nroll")

/* Requires: $http and $q objects to function */
.service("User", ["$rootScope", "$http", "$q", "TokenFactory", "$log", function ($rootScope, $http, $q, TokenFactory, $log, ERROR) {
    /** 
     * Define User function constructor
     * All user objects will be based on this blueprint
     */

    this.firstName = "";
    this.lastName = "";
    this.email = "";
    this.applications = 0;
    this.loggedIn = "";

    /**
     * Getter method returns user's first name
     */
    this.getFirstName = function () {
        return this.firstName || null;
    };

    /**
     * Getter method returns user's last name
     */
    this.getLastName = function () {
        return this.lastName || null;
    };


    /**
     * Getter method to return User's full name
     * @return full name
     */
    this.getFullName = function () {
        return this.userInfo.firstName + " " + this.userInfo.lastName;
    };

    /**
     * Getter method to return loggedIn staus 
     * @return Boolean 
     */
    this.isLoggedIn = function () {
        return this.loggedIn;
    };



    /**
     * Setter method to set user login status
     * Will primarily be used by AuthFactory
     */
    this.setLoginStatus = function (status) {
        this.loggedIn = status || false;
    };

    /**
     * Setter method to set User object
     * @param user object with details: firstName, lastName & email
     */
    this.setUser = function (user) {
        /* Check if user object contains required details */
        if (user.firstName && user.lastName && user.email) {
            this.firstName = user.firstName;
            this.lastName = user.lastName;
            this.email = user.email;
        } else {
            $log.error(ERROR.invalidObject + "User object provided is missing either firstName, lastName or email fields");
        }
    };


}])