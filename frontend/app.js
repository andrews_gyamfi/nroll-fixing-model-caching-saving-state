/**
 * Main application module
 */
var app = angular.module("nroll", ["ui.router", "satellizer", "ngAnimate", "ui.select", "ngSanitize", "angular-spinkit", "ngFileUpload", "ui.bootstrap", "ngStorage", "angular-loading-bar"])

/**
 * Set application routes
 */
.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $authProvider, uiSelectConfig, cfpLoadingBarProvider) {

    // set default route to login page

    /*    $urlRouterProvider.otherwise("/");*/

    $urlRouterProvider.otherwise(function ($injector, $location) {
        $injector.invoke(['$state', "$auth", "$rootScope", function ($state, $auth, $rootScope) {
            if ($auth.isAuthenticated()) {
                $state.go('app.home');
            } else {
                $state.go('login');
            }
        }]);
    });

    // set routes with stateProvider
    $stateProvider
        .state("header", {
            templateUrl: "/views/header.html",
            controller: "headerCtrl",
            controllerAs: "header"
        })
        .state("login", {
            url: "/",
            templateUrl: "/views/login.html",
            controller: "mainCtrl",
            controllerAs: "main",
            data: {
                authenticate: false
            }

        })
        .state("logout", {
            url: "/logout",
            controller: function ($rootScope, $state, $auth) {
                $auth.logout();
                $rootScope.authenticated = false;
                $state.go("login", {
                    reload: true
                });
            },
            data: {
                authenticate: false
            }
        })
        .state("register", {
            url: "/register",
            templateUrl: "/views/register.html",
            controller: "userCtrl",
            controllerAs: "user",
            data: {
                authenticate: false
            }
        })
        .state("forgot", {
            url: "/forgot",
            templateUrl: "/views/forgot.html",
            controller: "resetPasswordCtrl",
            controllerAs: "ctrl",
            data: {
                authenticate: false
            }
        })
        .state("app", {
            url: "/app",
            abstract: true,
            template: "<ui-view/>",
            resolve: {
                auth: ["$q", "$rootScope", "AuthFactory", authResolve]
            },
            data: {
                authenticate: true
            }
        })
        .state("app.home", {
            url: "/home",
            templateUrl: "/views/dashboard.html",
            controller: "dashboardCtrl",
            controllerAs: "dashboard",
            resolve: {
                myapplications: ["$q", "$http", getMyApplications]
            },
            data: {
                authenticate: true
            }
        })
        .state("app.apply", {
            url: "/apply",
            abstract: true,
            templateUrl: "/views/application.html",
            data: {
                authenticate: true
            }
        })
        .state("app.apply.selectCourse", {
            url: "/course",
            templateUrl: "/views/selectCourse.html",
            controller: "selectCourseCtrl",
            controllerAs: "ctrl",
            resolve: {
                intakes: ["$q", "CourseFactory", resolveIntakes],
                countries: ["$http", "$q", countriesDataResolve],
                referenceData: ["$http", "$q", referenceDataResolve]
            },
            data: {
                authenticate: true
            }
        })
        .state("app.apply.eligibility", {
            url: "/eligibility",
            templateUrl: "/views/eligibility.html",
            data: {
                authenticate: true
            }
        })
        .state("app.apply.completeDetails", {
            url: "/details",
            templateUrl: "/views/apply.html",
            controller: "applyCtrl",
            controllerAs: "apply",
            resolve: {
                referenceData: ["$http", "$q", referenceDataResolve],
                countries: ["$http", "$q", countriesDataResolve],
                languages: ["$http", "$q", languagesDataResolve]
            },
            data: {
                authenticate: true
            }

        })
        .state("app.apply.upload", {
            url: "/uploads",
            templateUrl: "/views/upload.html",
            controller: "uploadCtrl",
            controllerAs: "upload",
            data: {
                authenticate: true
            }
        })
        .state("app.apply.declaration", {
            url: "/declaration",
            templateUrl: "/views/declaration.html",
            controller: "declarationCtrl",
            controllerAs: "ctrl",
            data: {
                authenticate: true
            }
        })

    /* Normalize url */
    //$locationProvider.html5Mode(true);

    /* Setup Satellizer */
    $authProvider.loginUrl = "/api/auth/login";
    $authProvider.signupUrl = "api/auth/register";

    /*$locationProvider.html5Mode(true);*/
    //    $httpProvider.interceptors.push("AuthInterceptor");

    /* Configur uiSelect */
    uiSelectConfig.theme = 'bootstrap';
    uiSelectConfig.resetSearchInput = true;
    uiSelectConfig.appendToBody = true;


    /* Angular loading spinner */
    cfpLoadingBarProvider.includeSpinner = false;

    /* Method resolves reference JSON data for application form */
    function referenceDataResolve($http, $q) {
        return $http.get("./json/references.json", {
                cache: true
            })
            .then(function (response) {
                /* If successfully resolved, return data for use by state controller */
                return response.data;
            }).catch(function (response) {
                /* If not successful, reject promise */
                $q.reject();
            })
    }

    /* Method resolves countries JSON data for application form */
    function countriesDataResolve($http, $q) {
        return $http.get("./json/countries.json", {
                cache: true
            })
            .then(function (response) {
                /* If successfully resolved, return data for use by state controller */
                return response.data;
            })
            .catch(function (response) {
                /* If not successful, reject promise */
                $q.reject();
            })
    }

    /* Method resolves languages JSON data for application form */
    function languagesDataResolve($http, $q) {
        return $http.get("./json/languages.json", {
                cache: true
            })
            .then(function (response) {
                /* If successfully resolved, return data for use by state controller */
                return response.data;
            })
            .catch(function (response) {
                /* If not successful, reject promise */
                $q.reject();
            })
    }


    /* Method resolves courses for Dashboard */
    function coursesResolve($q, CourseFactory) {
        return CourseFactory.getCourses()
            .then(function (courseList) {
                /* If successfully resolved, return data for use by state controller */
                return courseList;
            }).catch(function (response) {
                /* If not successful, reject promise */
                $q.reject();
            });
    }

    /* Method resolves intakes data from salesforce */
    function resolveIntakes($q, CourseFactory) {
        return CourseFactory.getIntakes()
            .then(function (intakes) {
                /* If successfully resolved, return data for use by state controller */
                return intakes;
            })
            .catch(function (response) {
                console.log(response);
                /* If not successful, reject promise */
                return $q.reject();
            });
    }

    /* Method checks if user authenticated */
    function authResolve($q, $rootScope, AuthFactory) {
        return AuthFactory.getUser();
    };

    /* Method gets user applications if any */
    function getMyApplications($q, $http) {
        return $http.get("api/application/myapplications")
            .then(function (response) {
                console.log(response);
                return response.data.applications;
            })
            .catch(function (response) {
                console.log(response);
                $q.reject();
            })
    }


});

/**
 * Set Application constants
 */
app.constant("AUTH_EVENTS", {
    LOGGEDIN: "loggedIn",
    LOGGEDOUT: "logout",
    REFRESHED: "refreshed",
    USER_CREATED: "userCreated"
})


app.constant("UI_ROUTER_EVENTS", {
    STARTED: "stateChangeStarted",
    SUCCESS: "stateChangeSuccess"
})

/**
 * Set application run settings
 */
.run(["$rootScope", "$state", "User", "TokenFactory", "$http", "$auth", "UI_ROUTER_EVENTS", "AuthFactory", function ($rootScope, $state, User, TokenFactory, $http, $auth, UI_ROUTER_EVENTS, AuthFactory) {

    /* Set empty user object on $rootScope */
    $rootScope.currentUser = {};

    /* Initialize Fastclick */
    FastClick.attach(document.body);

    /* Handle route state change */
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams, options) {

        /* Register event on $rootScope for loading animation */
        $rootScope.stateIsLoading = true;
        $rootScope.isLoading = true;


        /* Redirect to dashboard as home if user is already logged in */
        if ($auth.isAuthenticated() && (toState.name === "login" || toState.name === "register" || toState.url === "/")) {
            event.preventDefault();
            return;
        }


        /* If user is not logged in, redirect to homepage */
        if (toState.data.authenticate && !$auth.isAuthenticated()) {
            event.preventDefault();
            $state.go("login");
        }


    });

    /* State route event */
    $rootScope.$on("$stateChangeSuccess", function () {
        /* Register event on $rootScope for loading animation */
        $rootScope.stateIsLoading = false;

    });

    /* State change error event */
    $rootScope.$on("$stateChangeError", function (event, current, previous, eventObj) {
        $rootScope.stateIsLoading = false;
    });


}]);